# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Eloq Portal Integration',
    'version': '1.0',
    'author': 'Eloquentia Solutions S.A de CV',
    'category' : 'tools',
    "website": "http://www.eloquentia.com.mx/",
    "depends": ['base'],
    'description' : """
        This module handle request for register cron events to be triggered 
        when a contract expires by date the a call to the portal is made to 
        communicate the events when happens
    """,
    "init_xml": [],
    "update_xml": [],
    'js': [
        'static/src/js/eloq_portal.js'
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
    'application': True
}
