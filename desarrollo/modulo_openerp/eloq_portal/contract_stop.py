# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public Licensed
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xmlrpclib
from openerp import tools
from dateutil.relativedelta import relativedelta
import datetime

'''
Created on 19/08/2013

@author: aek
'''

from osv import fields, osv


class contract_stop(osv.osv):
    _name = "eloq.contract_stop"

    _columns = {
        'name' : fields.char ('Instance Name', size=250, required=True),
        'contract_id' : fields.integer('Contract Id', required=True),
        'user_ids' : fields.many2many('res.users', 'res_users_contract_rel', 'uid', 'cid', 'Users'),
        'end_date' : fields.datetime('Expiration date'),
        'interval': fields.integer('Interval'),
        'cron_id' : fields.many2one('ir.cron', 'Expire Task', help="This cron task will trigger RPC calls to the portal to notify contract expiration events"),
        'state': fields.selection([
            ('demo', 'Demo'),
            ('active', 'Active'),
            ('cancel', 'Cancelled'),
            ('extra', 'Extra')
        ], 'State', size=16),
    }
    
    def expire_warning(self, cr, uid, context=None):
        config_obj = self.pool.get('ir.config_parameter')
        portal_url = config_obj.get_param(cr, uid, 'portal.server.url', default='http://localhost:8000/xmlrpc/')
        portal_user = config_obj.get_param(cr, uid, 'portal.server.user', default='admin')
        portal_password = config_obj.get_param(cr, uid, 'portal.server.password', default='admin')
        obj_conn = xmlrpclib.ServerProxy(portal_url)
        stop_ids = self.search(cr, uid, [], context=context)
        stop_obj = self.browse(cr, uid, stop_ids, context=context)[0]
        if stop_obj.state == 'demo':
            return self.expire(cr, uid, context=context)
        cron_pool = self.pool.get('ir.cron')
        end_date = stop_obj.end_date + relativedelta(months=1)
        cron_pool.write(cr, uid, stop_obj.cron_id.id, {'nextcall': end_date, 'numbercall': 1}, context=None)
        obj_conn.contract_expire_warning(portal_user, portal_password, stop_obj.contract_id, 'extra')
        return True
    
    def expire(self, cr, uid, context=None):
        config_obj = self.pool.get('ir.config_parameter')
        portal_url = config_obj.get_param(cr, uid, 'portal.server.url', default='http://localhost:8000/xmlrpc/')
        portal_user = config_obj.get_param(cr, uid, 'portal.server.user', default='admin')
        portal_password = config_obj.get_param(cr, uid, 'portal.server.password', default='admin')
        obj_conn = xmlrpclib.ServerProxy(portal_url)
        cron_pool = self.pool.get('ir.cron')
        user_pool = self.pool.get('res.users')
        stop_ids = self.search(cr, uid, [], context=context)
        stop_obj = self.browse(cr, uid, stop_ids, context=context)[0]
        cron_pool.write(cr, uid, [stop_obj.cron_id.id], {'active': False}, context=None)
        user_ids = [user.id for user in stop_obj.user_ids]
        user_pool.write(cr, uid, user_ids, {'active': False}, context=None)
        obj_conn.contract_expire_warning(portal_user, portal_password, stop_obj.contract_id, 'cancel')
        return True
    
        
    def pay_excent(self, cr, uid, context=None):
        cron_pool = self.pool.get('ir.cron')
        stop_ids = self.search(cr, uid, [], context=context)
        cron_ids = [stop.cron_id.id for stop in self.browse(cr, uid, stop_ids, context=context)]
        cron_pool.write(cr, uid, cron_ids,{'active': False}, context=context)
        return True
    
    def create(self, cr, uid, vals, context=None):
        cron_pool = self.pool.get('ir.cron')
        method = 'expire_warning'
        if 'state' in vals and vals['state'] == 'demo':
            method = 'expire'
        cron_id = cron_pool.create(cr, uid, {
            'name': 'Expire task for instance %s'%(vals['name']),
            'nextcall' : vals['end_date'],
            'interval_number' : vals['interval'],
            'numbercall' : -1,
            'model': "eloq.contract_stop",
            'function': method,
        }, context=context)
        vals.update({'cron_id': cron_id})
        return super(contract_stop, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        cron_pool = self.pool.get('ir.cron')
        method = 'expire_warning'
        if 'state' in vals and vals['state'] == 'extra':
            method = 'expire'
        if 'end_date' in vals:
            stop = self.browse(cr, uid, ids, context=context)
            cron_ids = [stop.cron_id.id]
            cron_pool.write(cr, uid, cron_ids, {
                'nextcall' : vals['end_date'],
                'numbercall': -1,
                'function': method,
                'active': True
            }, context=context)
        return super(contract_stop, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        user_pool = self.pool.get('res.users')
        cron_pool = self.pool.get('ir.cron')
        
        stop_ids = self.search(cr, uid, [], context=context)
        stop_obj = self.browse(cr, uid, stop_ids, context=context)[0]
        
        user_ids = [user.id for user in stop_obj.user_ids]
        user_pool.write(cr, uid, user_ids, {'active': False}, context=None)
        
        cron_ids = [stop.cron_id.id for stop in self.browse(cr, uid, ids, context=context)]
        cron_pool.unlink(cr, uid, cron_ids, context=context)
        return super(contract_stop, self).unlink(cr, uid, ids, context=context)
    
contract_stop()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: