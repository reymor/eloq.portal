'''
Created on 21/08/2013

@author: aek
'''
from openerp.addons.web.controllers.main import Database, openerpweb
import re

class Database(Database):
    _cp_path = "/web/database"

    @openerpweb.jsonrequest
    def get_list(self, req, app):
        proxy = req.session.proxy("db")
        dbs = proxy.list()
        h = req.httprequest.environ['HTTP_HOST'].split(':')[0]
        d = h.split('.')[0]
        r = req.config.dbfilter.replace('%h', h).replace('%d', d)
        dbs = [i for i in dbs if re.match(r, i)]
        if app and app in dbs:
            dbs = [app]
        else:
            dbs = []
        return {"db_list": dbs}