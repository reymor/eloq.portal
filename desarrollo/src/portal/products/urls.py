from django.conf.urls import url, patterns
#from django.views.generic.base import RedirectView

from registration.views import register
from django.contrib.auth import views as auth_views

from django.views.generic.list import ListView
from models import Product
from forms import PortalRegistrationForm

from django_countries.countries import COUNTRIES

from config import portal_config

product_defaults = {'default_url': 'products.html'}

urlpatterns = patterns('',
    url(r'^accounts/register/$',register, {'backend': 'registration.backends.simple.SimpleBackend', 'success_url':"/accounts/profile/", 'form_class': PortalRegistrationForm, 'extra_context':{'countries': COUNTRIES}}),
    url(r'^profiles/edit/$', 'products.views.update_user_profile'),
    url(r'^profiles/password/change/$', auth_views.password_change,{'template_name': 'profiles/password_change.html','post_change_redirect': '/accounts/profile/'},name='auth_password_change'),
    url(r'^profiles/password/reset/$',auth_views.password_reset,{'template_name':'profiles/password_reset.html'},name='auth_password_reset'),
    url(r'^profiles/bonus\.json$', 'products.views.list_profile_bonus', {'template_name': 'profiles/bonus.json'}),
    url(r'^profiles/payments\.json$', 'products.views.list_profile_payments', {'template_name': 'profiles/payments.json'}),
    
    url(r'^products\.html$', ListView.as_view(template_name= 'products/list.html', model = Product)),
    url(r'^products/(?P<slug>\w+)$', 'products.views.products_details_view', {'template_name': 'products/details.html', 'default_url': '/products.html'}),
    url(r'^products/share\.html$', 'products.views.product_share_view', {'template_name': 'products/share.html'}),
    
    url(r'^products/plan_config\.html$', 'products.views.product_config_view', {'template_name': 'products/plan_config.html'}),
    url(r'^products/contract_build\.html$', 'products.views.product_contract_build_view', {'template_name': 'products/contract.html'}),
    url(r'^products/plans\.json$', 'products.views.product_plan_view', {'template_name': 'products/plans.json'}),
    
    url(r'^products/contracts\.json$', 'products.views.list_product_contracts', {'template_name': 'products/contracts.json'}),
    url(r'^products/contract/(?P<contract_id>\d+)/$', 'products.views.product_contract_view', {'template_name': 'products/contract.html'}),
    url(r'^products/contract/cancel\.html$', 'products.views.product_contract_cancel', {'template_name': 'profiles/profile_detail.html'}),
    
    url(r'^products/instances\.json$', 'products.views.list_product_instances', {'template_name': 'products/instances.json'}),
    url(r'^products/instance/(?P<instance_id>\d+)/$', 'products.views.product_instance_view', {'template_name': 'products/instance.html'}),
    url(r'^products/instance/cancel\.html$', 'products.views.product_instance_cancel', {'template_name': 'products/contract.html'}),
    
    url(r'^products/users\.json$', 'products.views.list_product_users', {'template_name': 'products/users.json'}),
    url(r'^products/user/(?P<user_id>\d+)/$', 'products.views.product_user_view', {'template_name': 'products/user.html'}),
    url(r'^products/user/cancel\.html$', 'products.views.product_user_cancel', {'template_name': 'products/instance.html'}),
    url(r'^products/user/password\.html$', 'products.views.product_user_password', {'template_name': 'products/user_password.html'}),
    url(r'^products/user/password/change\.html$', 'products.views.product_user_password_change', {'template_name': 'products/user.html'}),
    
    url(r'^products/contract_demo\.html$', 'products.views.product_demo_view', {'template_name': 'products/contract.html'}),
    
    url(r'^products/pay/(?P<contract_id>\d+)/$', 'products.views.product_contract_pay_view', {'template_name': 'products/pay.html'}),
    
    url(r'^products/return/$', 'products.views.product_return_view', {'template_name': 'products/return.html'}),
    url(r'^products/cancel/$', 'products.views.product_cancel_view', {'template_name': 'products/cancel.html'}),
)