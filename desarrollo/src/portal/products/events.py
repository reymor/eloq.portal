import xmlrpclib
import datetime
from dateutil.relativedelta import relativedelta

from django.contrib.auth.models import User
from django.db.models.base import ModelBase
from django.db.models.signals import pre_save
from django.conf import settings
from django.template import loader
from django.core.mail import send_mail

from registration.signals import user_registered
from registration.backends.simple import SimpleBackend

from paypal.standard.ipn.signals import payment_was_successful

from django_xmlrpc.decorators import xmlrpc_func, permission_required

from models import PortalProfile, ProductPlan, ProductContract, ProductContractPayment, ProductShare, ProfileBonus
from forms import PortalRegistrationForm, ProductContractForm
from config import portal_config
from django.core.mail.message import EmailMultiAlternatives



login_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/common')
obj_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/object')


def create_profile(sender, **kw):
    user = kw["user"]
    request = kw["request"]
    form = PortalRegistrationForm(data=request.POST, files=request.FILES)
    form.full_clean()
    user.first_name = form.cleaned_data['first_name']
    user.last_name = form.cleaned_data['last_name']
    user.save()
    profile = PortalProfile(user=user, country = form.cleaned_data['country'], phone = form.cleaned_data['phone'])
    profile.save()

user_registered.connect(create_profile, sender=SimpleBackend)

def update_profile(sender, **kw):
    profile = kw["instance"]
    if profile.pay_excent:
        for contract in profile.contracts.all():
            for instance in contract.instances.all():
                uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
                obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'pay_excent')
            contract.end_date = None
            contract.save()
            
pre_save.connect(update_profile, sender=PortalProfile)

def save_payment(sender, **kw):
    payment = kw["instance"]
    contract = payment.contract
    contract.ammount = contract.ammount + payment.ammount
    today = datetime.datetime.today()
    if contract.state == 'draft' or contract.state == 'demo':
        c = {'contract': contract.id, 'demo': False}
        shares = ProductShare.objects.filter(product=contract.product, email=contract.profile.user.email, active=True)
        for share in shares:
            end_date = today + relativedelta(months=contract.product.bonus_end)
            bonus = ProfileBonus(profile=share.profile, product=contract.product, init=today, end=end_date, active=True)
            bonus.save()
            share.active = False
            share.save()
        form = ProductContractForm(c)
        form.full_clean()
        form.save()
    else:
        if contract.end_date == None:
            contract.end_date = today + relativedelta(months=contract.months)
        else:
            contract.end_date = contract.end_date + relativedelta(months=contract.months)
        for instance in contract.instances.all():
            if instance.state != 'cancel':
                uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
                if contract.state == 'cancel':
                    user_ids = [no_user.user_id for no_user in instance.users.all() if no_user.state == 'cancel']
                    obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.users', 'write', user_ids, {'active': True})
                if not contract.profile.pay_excent:
                    for no_user in instance.users.all():
                        if no_user.state != 'cancel':
                            no_user.state = 'active'
                            no_user.save()
                    stop_data = {'state': 'active', 'end_date': datetime.datetime.strftime(contract.end_date, '%Y-%m-%d %H:%M:%S')}
                    obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'write', instance.stop_id, stop_data)
        contract.state = 'active'
        contract.save()
        send_notification(contract)
        
pre_save.connect(save_payment, sender=ProductContractPayment)

def confim_payment(sender, **kwargs):
    ipn_obj = sender
    contract = ProductContract.objects.get(id=ipn_obj.common)
    if contract:
        today = datetime.datetime.today()
        payment = ProductContractPayment(contract=contract, source='paypal', pay_date=today, details=ipn_obj.txn_id)
        payment.save()
                
payment_was_successful.connect(confim_payment)

def send_notification(contract):
    #correo de notificacion
    c = {
        'product': contract.product,
        'contract': contract,
        'name': contract.profile.user.get_full_name()
    }
    subject = loader.render_to_string(portal_config.get(contract.state).get('subject_template'), c)
    subject = ''.join(subject.splitlines())
    message = loader.render_to_string(portal_config.get(contract.state).get('email_template'), c)
    msg = EmailMultiAlternatives(subject, message, None, [contract.profile.user.email])
    msg.attach_alternative(message, "text/html")
    msg.send(True)
#     send_mail(subject, message, None, [contract.profile.user.email],fail_silently=True)

@permission_required(perm=None)
@xmlrpc_func(returns='boolean', args=['integer', 'string', 'string', 'string'])
def contract_expire_warning(user, contract_id, state):
    contract = ProductContract.objects.get(id=contract_id)
    if contract and contract.state != state:
        contract.state = state
        contract.save()
        send_notification(contract)
    