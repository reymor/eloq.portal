portal_config = {
    'create': {
        'template': 'products/contract.html', 'email_template': 'products/mails/contract_email.html', 'subject_template': 'products/mails/contract_email_subject.txt'
    },
    'cancel': {
        'template': 'products/contract.html', 'email_template': 'products/mails/contract_cancel_email.html', 'subject_template': 'products/mails/contract_cancel_email_subject.txt'
    },
    'active': {
        'template': 'products/contract.html', 'email_template': 'products/mails/contract_active_email.html', 'subject_template': 'products/mails/contract_active_email_subject.txt'
    },
    'warning': {
        'email_template': 'products/mails/contract_warning_email.html', 'subject_template': 'products/mails/contract_warning_email_subject.txt'
    },
    'share': {
        'email_template': 'products/mails/share_email.html', 'share_complete_template': 'products/share_complete.html', 'subject_template': 'products/mails/share_email_subject.txt'
    }
}