from django.conf import settings
from django import http

class AjaxLoadPageMiddleware(object):
    def process_request(self, request):
        if request.path in settings.NON_AJAX_VIEWS or request.path.startswith(settings.MEDIA_URL) or request.path.startswith("/admin") or request.path.startswith("/support") or request.path.startswith("/offline"):
            return None
        if request.META.get('HTTP_X_REQUESTED_WITH') != 'XMLHttpRequest':
            return http.response.HttpResponseRedirect('/?redirect='+request.path)
        return None