import os, time, random, json
import xmlrpclib, urllib2, cookielib
import datetime
from dateutil.relativedelta import relativedelta

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm

from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.mail import send_mail

from django.db.models import F

from django.template import loader
from django.template.context import RequestContext
from django.template.response import TemplateResponse

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.decorators.http import require_POST
from django.views.decorators.debug import sensitive_post_parameters

from paypal.standard.ipn.models import PayPalIPN
from paypal.standard.ipn.forms import PayPalIPNForm
from paypal.standard.forms import PayPalPaymentsForm

from models import Product, ProductContract, ProductContractPayment, ProductContractInstance, ProductContractInstanceUser, ProductShare, ProductPlan, Contact_Entry, SlideShow
from products.forms import ProductBuyForm, PortalRegistrationForm, ProductShareForm, ProductContractForm
from profiles.views import edit_profile
from admin import disable_contract, disable_instance, disable_user, change_password

from config import portal_config
from django.utils.crypto import get_random_string

from django_countries.countries import COUNTRIES
from django.core.mail.message import EmailMultiAlternatives

contact_entry = None

def home_view(request, template_name):
    prods = Product.objects.all()
    products = []
    index = 0
    buf = []
    for prod in prods:
        buf.append(prod)
        index+=1
        if index is 4:
            products.append(buf)
            buf = []
            index = 0
    if buf:
        products.append(buf)
    slides = SlideShow.objects.all()
    
    return render_to_response(template_name, RequestContext(request,{
        'products': prods,
        'slides': slides
    }))

def products_details_view(request, slug, template_name, default_url):
    if slug:
        product = Product.objects.get(slug=slug)
        return render_to_response(template_name, RequestContext(request,{
            'product': product
        }))
    return HttpResponseRedirect(default_url)

@login_required(login_url='/accounts/login_required.html')
def update_user_profile(request):
    if request.method == 'POST':
        data=dict([item for item in request.POST.iteritems()])
        data['password1'] = request.user.password
        data['password2'] = request.user.password
        form = PortalRegistrationForm(data=data, files=request.FILES)
        form.full_clean()
        
        user = User.objects.get(id=request.user.id)
        user.username = data['username']
        user.email = data['email']
        user.first_name = data['first_name']
        user.last_name = data['last_name']
        if form.errors.has_key('username') and data.get('username') == request.user.username:
            form.errors.pop('username')
        
        if form.errors.has_key('email') and data.get('email') == request.user.email:
            form.errors.pop('email')
            
        if form.is_valid():
            user.save()
            request.user = user
        else:
            default_url = reverse('profiles_profile_detail',
                              kwargs={ 'username': request.user.username })
            return HttpResponseRedirect(default_url)
    default_url = reverse('profiles_profile_detail',
                              kwargs={ 'username': request.user.username })
    return edit_profile(request, success_url=default_url, extra_context={'countries': COUNTRIES})

@sensitive_post_parameters()
@csrf_protect
@login_required(login_url='/accounts/login_required.html')
def password_change(request,
                    template_name='profiles/password_change.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    current_app=None, extra_context=None):
    if post_change_redirect is None:
        post_change_redirect = reverse('profiles_profile_detail',
                              kwargs={ 'username': request.user.username })
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(post_change_redirect)
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)
    
def profile(request, template_name):
    return render_to_response(template_name, {
        'user': request.user.username
    })

@login_required(login_url='/accounts/login_required.html')
def product_share_view(request, template_name):
    if request.method == "POST":
        product = Product.objects.get(id=request.POST.get('product', None))
        form = ProductShareForm(request.POST)
        form.full_clean()
        if form.is_valid():
            c = {
                'email': form.cleaned_data['email'],
                'user': request.user,
                'product': form.cleaned_data['product'],
                'name': request.user.get_full_name()
            }
            subject = loader.render_to_string(portal_config.get('share').get('subject_template'), c)
            subject = ''.join(subject.splitlines())
            message = loader.render_to_string(portal_config.get('share').get('email_template'), c)
            msg = EmailMultiAlternatives(subject, message, None, [form.cleaned_data['email']])
            msg.attach_alternative(message, "text/html")
            msg.send(True)
            #send_mail(subject, message, None, [form.cleaned_data['email']],fail_silently=True)
            share = ProductShare(profile = request.user.profile, product = form.cleaned_data['product'], email = form.cleaned_data['email'], active=True)
            share.save()
            return render_to_response(portal_config.get('share').get('share_complete_template'), RequestContext(request,{}))
        else:
            return render_to_response(template_name, RequestContext(request,{
                'form': form,
                'product': product
            }))
    else:
        product = Product.objects.get(id=request.GET.get('product', None))
    return render_to_response(template_name, RequestContext(request,{
        'product': product
    }))
        

def contact_view(request, template_name):
#    if contact_entry is None:
    contact_entry = Contact_Entry.objects.get(id=1)
    
    return render_to_response(template_name, RequestContext(request, {
        'Contact_Entry': contact_entry,
        'modal': not request.GET.get('modal', False)
    }))

@require_POST
def product_plan_view(request, template_name):
    idProd = request.POST.get('id')
    prod = Product.objects.get(id=idProd)
    plans = prod.plans.all()
    return render_to_response(template_name, RequestContext(request, {
        'plans': plans,
        'count': len(plans)
    }))

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_demo_view(request, template_name):
    idContract = request.POST.get('id')
    c = {'contract': idContract, 'demo': True}
    form = ProductContractForm(c)
    if form.is_valid():
        form.save()
        return render_to_response(template_name, RequestContext(request, {
            'contract': form.cleaned_data['contract'],
        }))
    post_change_redirect = reverse('profiles_profile_detail',
                              kwargs={ 'username': request.user.username })
    return HttpResponseRedirect(post_change_redirect)

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_config_view(request, template_name):
    idPlan = request.POST.get('id')
    plan = ProductPlan.objects.get(id=idPlan)
    if plan:
        plan_data = [[index for index in range(0,plan.users)] for inst in range(0,plan.instances)] 
        return render_to_response(template_name, RequestContext(request, {
            'plan': plan,
            'data': plan_data
        }))

@login_required(login_url='/accounts/login_required.html')
def product_contract_pay_view(request, contract_id, template_name):
    if contract_id:
        profile = request.user.profile
        today = datetime.date.today()
        contract = ProductContract.objects.get(id=contract_id, profile=profile)
        bonuses = profile.bonus.filter(product = contract.product, init__gte = today, end__gte = today, active = True).count()
        bonus = contract.product.bonus * bonuses
        if bonus > 100:
            bonus = 1
        else:
            bonus = bonus/100
        contract.discount = bonus
        contract.save()
        price = contract.price - (contract.price*contract.discount)
        invoice = get_random_string()
        paypal_dict_buy = {
           "cmd": "_xclick",
           "business": settings.PAYPAL_RECEIVER_EMAIL,
           "item_name": contract.product.slug,
           "common": str(contract.id),
           "invoice": invoice,
           "amount": price,
           "notify_url": "%s%s" % (settings.SITE_NAME, reverse('paypal-ipn')),
           "return_url": "%s/profiles/contract/%d" % (settings.SITE_NAME, contract.id),
           "cancel_return": "%s/products/cancel/" % (settings.SITE_NAME),
        }
        paypal_dict_subscribe = {
           "cmd": "_xclick-subscriptions",
           "business": settings.PAYPAL_RECEIVER_EMAIL,
           "item_name": contract.product.slug,
           "common": str(contract.id),
           "invoice": invoice,
           "a3": price,
           "p3": contract.months,
           "t3": "M",
           "amount": price,
           "src": "1",
           "sra": "1",
           "no_note": "1",
           "notify_url": "%s%s" % (settings.SITE_NAME, reverse('paypal-ipn')),
           "return_url": "%s/profiles/contract/%d/" % (settings.SITE_NAME, contract.id),
           "cancel_return": "%s/products/cancel/" % (settings.SITE_NAME),
        }
        form_buy = PayPalPaymentsForm(initial=paypal_dict_buy)
        form_subscribe = PayPalPaymentsForm(button_type="subscribe",initial=paypal_dict_subscribe)
        if settings.DEBUG == True:
            form_buy = form_buy.sandbox()
            form_subscribe = form_subscribe.sandbox()
        else:
            form_buy.render()
            form_subscribe = form_subscribe.render()
        return render_to_response(template_name, RequestContext(request, {
            'contract': contract,
            "form_buy": form_buy,
            "form_subscribe": form_subscribe,
            "price": price
        }))

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_contract_build_view(request, template_name):
    user = request.user
    idPlan = request.POST.get('id')
    plan = ProductPlan.objects.get(id=idPlan)
    if plan:
        plan_config = {}
        try:
            for inst in range(0,plan.instances):
                cfg = []
                for index in range(0,plan.users):
                    usr_name = request.POST.get('user'+str(index)+'_instance'+str(inst+1), False)
                    if usr_name:
                        cfg.append(usr_name)
                    else:
                        raise Exception(None)
                inst_name = request.POST.get('instance'+str(inst+1), False)
                if inst_name:
                    plan_config[inst_name] = cfg
                else:
                    raise Exception(None)
        except Exception:
            return None
        
        today = datetime.date.today()
        profile = request.user.get_profile()
        bonuses = profile.bonus.filter(product = plan.product, init__gte = today, end__gte = today, active = True).count()
        bonus = plan.product.bonus * bonuses
        if bonus > 100:
            bonus = 1
        else:
            bonus = bonus/100
        
        contract = ProductContract(profile = user.profile, product=plan.product, ammount=0.0, discount=bonus, price=plan.price, months=plan.months, init_date=None, end_date=None, state='draft')
        contract.save()
        for (key, value) in plan_config.iteritems():
            db_name = 'eloq_%s_%s_%s'%(plan.product.slug, user.email, key)
            instance = ProductContractInstance(db_name=db_name, contract=contract, state='draft')
            instance.save()
            instance_user = ProductContractInstanceUser(username='app_admin', instance=instance, app_admin=True, state='draft')
            instance_user.save()
            for usr in value:
                instance_user = ProductContractInstanceUser(username=usr, instance=instance, app_admin=False, state='draft')
                instance_user.save()
        if profile.pay_excent:
            c = {'contract': contract.id, 'demo': False}
            form = ProductContractForm(c)
            if form.is_valid():
                form.save()
        return render_to_response(template_name, RequestContext(request, {
            "contract": contract
        }))


@login_required(login_url='/accounts/login_required.html')
def list_product_contracts(request, template_name):
    profile = request.user.get_profile()
    contracts = profile.contracts.all()
    
    return render_to_response(template_name, RequestContext(request, {
        'contracts': contracts,
        'count': len(contracts)
    }))


@login_required(login_url='/accounts/login_required.html')
def product_contract_view(request, contract_id, template_name):
    if contract_id:
        contract = ProductContract.objects.get(id=contract_id, profile=request.user.profile)
        return render_to_response(template_name, RequestContext(request, {
            'contract': contract,
            'url_openerp': settings.URL_OPENERP 
        }))
    return HttpResponseRedirect("/products.html")

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_contract_cancel(request, template_name):
    id_contract = request.POST.get('id')
    if id_contract:
        contract = ProductContract.objects.get(id=id_contract, profile=request.user.profile)
        disable_contract(contract)
        return render_to_response(template_name, RequestContext(request, {
            'profile': contract.profile,
        }))
    return HttpResponseRedirect("/products.html")

@require_POST
@login_required(login_url='/accounts/login_required.html')
def list_product_instances(request, template_name):
    id_contract = request.POST.get('id')
    instances = []
    if id_contract:
        contract = ProductContract.objects.get(id=id_contract, profile=request.user.profile)
        instances = contract.instances.all()
    
    return render_to_response(template_name, RequestContext(request, {
        'instances': instances,
        'count': len(instances)
    }))


@login_required(login_url='/accounts/login_required.html')
def product_instance_view(request, instance_id, template_name):
    if instance_id:
        instance = ProductContractInstance.objects.get(id=instance_id, contract__profile=request.user.profile)
        return render_to_response(template_name, RequestContext(request, {
            'instance': instance,
            'url_openerp': settings.URL_OPENERP
        }))
    return HttpResponseRedirect("/products.html")

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_instance_cancel(request, template_name):
    id_instance = request.POST.get('id')
    if id_instance:
        instance = ProductContractInstance.objects.get(id=id_instance, contract__profile=request.user.profile)
        disable_instance(instance)
        return render_to_response(template_name, RequestContext(request, {
            'contract': instance.contract,
        }))
    return HttpResponseRedirect("/products.html")

@require_POST
@login_required(login_url='/accounts/login_required.html')
def list_product_users(request, template_name):
    id_instance = request.POST.get('id')
    users = []
    if id_instance:
        instance = ProductContractInstance.objects.get(id=id_instance, contract__profile=request.user.profile)
        users = instance.users.all()
    
    return render_to_response(template_name, RequestContext(request, {
        'users': users,
        'count': len(users)
    }))


@login_required(login_url='/accounts/login_required.html')
def product_user_view(request, user_id, template_name):
    if user_id:
        instance_user = ProductContractInstanceUser.objects.get(id=user_id, instance__contract__profile=request.user.profile)
        return render_to_response(template_name, RequestContext(request, {
            'instance_user': instance_user,
        }))
    return HttpResponseRedirect("/products.html")

@require_POST
@login_required(login_url='/accounts/login_required.html')
def product_user_cancel(request, template_name):
    id_user = request.POST.get('id')
    if id_user:
        user = ProductContractInstanceUser.objects.get(id=id_user, instance__contract__profile=request.user.profile)
        disable_user(user)
        return render_to_response(template_name, RequestContext(request, {
            'instance': user.instance,
        }))
    return HttpResponseRedirect("/products.html")

def product_user_password(request, template_name):
    id_user = request.POST.get('id')
    if id_user:
        user = ProductContractInstanceUser.objects.get(id=id_user, instance__contract__profile=request.user.profile)
        return render_to_response(template_name, RequestContext(request, {
            'instance_user': user,
        }))
    return HttpResponseRedirect("/products.html")

def product_user_password_change(request, template_name):
    id_user = request.POST.get('id')
    password = request.POST.get('new_password1')
    if id_user and password:
        user = ProductContractInstanceUser.objects.get(id=id_user, instance__contract__profile=request.user.profile)
        user.password = password
        if user.state == 'active':
            change_password(user)
        user.save()
        return render_to_response(template_name, RequestContext(request, {
            'instance_user': user,
        }))
    return HttpResponseRedirect("/products.html")

@login_required(login_url='/accounts/login_required.html')
def list_profile_bonus(request, template_name):
    profile = request.user.get_profile()
    today = datetime.date.today()
    profile.bonus.filter(end__lt = today, active = True).update(active = False)
    bonuses = profile.bonus.all()
    
    return render_to_response(template_name, RequestContext(request, {
        'bonuses': bonuses,
        'count': len(bonuses)
    }))

@login_required(login_url='/accounts/login_required.html')
def list_profile_payments(request, template_name):
    profile = request.user.get_profile()
    payments = ProductContractPayment.objects.filter(contract__profile = profile)
    
    return render_to_response(template_name, RequestContext(request, {
        'payments': payments,
        'count': len(payments)
    }))
    

def product_cancel_view(request, template_name):
    return HttpResponseRedirect("/products.html")

def product_return_view(request, template_name):
    return HttpResponseRedirect("/products.html")