import datetime
import xmlrpclib
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.core.mail import send_mail
from django.template import loader

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from models import Product, PortalProfile, ProductPlan, ProductContract, ProductImage, SlideShow, ProfileBonus, ProductShare, ProductContractInstanceUser, ProductContractInstance, ProductContractPayment
from config import portal_config
from django.core.mail.message import EmailMultiAlternatives

login_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/common')
obj_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/object')

def change_password(user):
    uid = login_conn.login(user.instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
    obj_conn.execute(user.instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.users', 'write', [user.user_id], {'password': user.password})

def disable_contract(contract):
    if contract.state not in ['extra', 'cancel', 'draft']:
        contract.state = 'extra'
        today = datetime.datetime.today()
        end_date = today + relativedelta(months = 1)
        data = {
            'end_date': datetime.datetime.strftime(end_date, '%Y-%m-%d %H:%M:%S'), 
            'state': 'extra'
        }
        for instance in contract.instances.all():
            instance.state = 'extra'
            uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
            obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'write', instance.stop_id, data)
            instance.save()
        data.update({
            'user': contract.profile.user,
        })
        subject = loader.render_to_string(portal_config.get('warning').get('subject_template'), data)
        subject = ''.join(subject.splitlines())
        message = loader.render_to_string(portal_config.get('warning').get('email_template'), data)
        msg = EmailMultiAlternatives(subject, message, None, [contract.profile.user.email])
        msg.attach_alternative(message, "text/html")
        msg.send(True)
#         send_mail(subject, message, None, [contract.profile.user.email],fail_silently=True)
        contract.save()
    if contract.state == 'draft':
        for instance in contract.instances.all():
            instance.users.all().delete()
            instance.delete()
        contract.delete()
    
def disable_user(user):
    if user.instance.contract.state == 'draft':
        user.delete()
    else:
        uid = login_conn.login(user.instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
        obj_conn.execute(user.instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.users', 'write', [user.user_id], {'active': False})

def disable_instance(instance):
    if instance.contract.state == 'draft':
        instance.users.all().delete()
        instance.delete()
    else:
        instance.state = 'cancel'
        instance.users.all().update(state='cancel')
        uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
        obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'unlink', [instance.stop_id])
        instance.save()

class ProductContractInstanceUserInline(admin.StackedInline):
    model = ProductContractInstanceUser
    extra = 0

class ProductContractInstanceInline(admin.StackedInline):
    model = ProductContractInstance
    extra = 0
    inlines = [ProductContractInstanceUserInline,]

class ProductContractPaymentInline(admin.StackedInline):
    model = ProductContractPayment
    extra = 0

class ProductContractAdmin(admin.ModelAdmin):
    list_display = ('profile','product', 'ammount', 'state', 'price', 'months', 'init_date', 'end_date')
    inlines = [ProductContractInstanceInline,ProductContractPaymentInline]
    
    def disable_contracts(modeladmin, request, queryset):
        for contract in queryset:
            disable_contract(contract)
    disable_contracts.short_description = "Cancelar Contratos"

    def extend_contracts(modeladmin, request, queryset):
        for contract in queryset:
            contract.end_date = contract.end_date + relativedelta(months=contract.months)
            contract.save()
    extend_contracts.short_description = "Extender vigencia de Contratos"

    def pay_contracts(modeladmin, request, queryset):
        for contract in queryset:
            contract.end_date = contract.end_date + relativedelta(months=contract.months)
            contract.save()
            today = datetime.datetime.today()
            payment = ProductContractPayment(contract=contract, source='manual', pay_date=today, details=request.user.username, ammount=contract.ammount)
            payment.save()
    pay_contracts.short_description = "Pago a mano de Contratos"
    
    actions = ['disable_contracts', 'extend_contracts', 'pay_contracts']

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0

class ProductPlanInline(admin.StackedInline):
    model = ProductPlan
    extra = 0
    
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','icon', 'modules', 'manual', 'bonus', 'bonus_end')
    inlines = [ProductImageInline, ProductPlanInline]

class ProfileInline(admin.StackedInline):
    model = PortalProfile
    extra = 0

class BonusInline(admin.StackedInline):
    model = ProfileBonus
    extra = 0

class ProfileContract(admin.StackedInline):
    model = ProductContract
    extra = 0
    
class PortalProfileAdmin(admin.ModelAdmin):
    inlines = [ProfileContract, BonusInline]
    list_display = ('user','country', 'phone')
    
class ProfileUserAdmin(UserAdmin):
    inlines = [ProfileInline]

class ProductSlideShowAdmin(admin.ModelAdmin):
    list_display = ('image','link')
    
class ProductShareAdmin(admin.ModelAdmin):
    list_display = ('email','profile','product','active')
    
admin.site.register(PortalProfile, PortalProfileAdmin)
admin.site.register(Product, ProductAdmin)
#admin.site.register(ProfileBonus)
admin.site.register(ProductShare,ProductShareAdmin)
admin.site.register(ProductContract, ProductContractAdmin)
admin.site.register(SlideShow, ProductSlideShowAdmin)
admin.site.unregister(User)
admin.site.register(User, ProfileUserAdmin)