import os
import urllib2
import xmlrpclib
import cookielib
import datetime
from dateutil.relativedelta import relativedelta

from django import forms
from django.forms.util import ErrorList
from django.forms.models import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.template import loader
from django.core.mail import send_mail

from registration.forms import RegistrationFormUniqueEmail
from products.models import Product, ProductPlan, ProductContract, ProductContractInstance, ProductContractInstanceUser

from config import portal_config
from django.utils.crypto import get_random_string
from django.core.mail.message import EmailMultiAlternatives

attrs_dict = { 'class': 'required' }
login_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/common')
db_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/db')
obj_conn = xmlrpclib.ServerProxy(settings.URL_OPENERP + '/xmlrpc/object')

class PortalRegistrationForm(RegistrationFormUniqueEmail):
    first_name = forms.CharField(widget=forms.TextInput(attrs=attrs_dict))
    last_name = forms.CharField(widget=forms.TextInput(attrs=attrs_dict))
    country = forms.CharField(widget=forms.TextInput(attrs=attrs_dict))
    phone = forms.CharField(widget=forms.TextInput(attrs=attrs_dict))



class ProductBuyForm(forms.Form):
    plan = forms.IntegerField()
    product = forms.IntegerField()

#    def clean_product(self):
#        product = Product.objects.filter(id=self.cleaned_data['product'])
#        if product:
#            return product
#        raise forms.ValidationError(_("El producto no es valido."))

    def clean_plan(self):
        prod = Product.objects.filter(id=self.cleaned_data['product'])
        plan = ProductPlan.objects.filter(id=self.cleaned_data['plan'])
        if plan and self.cleaned_data['plan'] in prod.plans:
            return plan
        raise forms.ValidationError(_("El plan no es valido."))



class ProductContractForm(forms.Form):
    contract = forms.IntegerField()
    demo = forms.BooleanField()

    def clean_contract(self):
        contract = ProductContract.objects.get(id=self.cleaned_data['contract'])
        if contract:
            return contract
        raise forms.ValidationError(_("El Contrato no es valido."))

    def save(self):
        contract = self.cleaned_data["contract"]
        if contract.state == 'draft' or contract.state == 'demo':

            filename = str(contract.product.backup)
            data = open(os.path.join(settings.MEDIA_ROOT, filename), mode='rb').read().encode('base64')
            instances = contract.instances.all()
            if self.data["demo"] and contract.state != 'demo':
                instances = [instances[0]]
                contract.state = 'demo'
                today = datetime.datetime.today()
                contract.init_date = today
                contract.end_date = today + relativedelta(months=contract.product.demo_end)
            else:
                contract.state = 'active'
                contract.init_date = datetime.datetime.today()
                contract.end_date = datetime.datetime.today() + relativedelta(months=contract.months)
            for instance in instances:
                #admin_group_names = [grp.strip() for grp in contract.product.admin_groups.strip().split(',')]
                #user_group_names = [grp.strip() for grp in contract.product.user_groups.strip().split(',')]
                #admin_group_ids = obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.groups', 'search', [('name', 'in', admin_group_names)])
                #user_group_ids = obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.groups', 'search', [('name', 'in', user_group_names)])

                if instance.state == 'demo':
                    uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
                    instance.state = contract.state
                    if not contract.profile.pay_excent:
                        stop_data = {
                            'contract_id': contract.id,
                            'end_date': datetime.datetime.strftime(contract.end_date, '%Y-%m-%d %H:%M:%S')
                        }
                        obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'write', instance.stop_id, stop_data)
                else:
                    user_ids = []
    #creacion de la instancia en openerp
                    db_conn.restore(settings.OPENERP_ADMIN_PASSWORD, instance.db_name, data)
                    uid = login_conn.login(instance.db_name, 'admin', settings.OPENERP_ADMIN_PASSWORD)
                    users = instance.users.all()
                    for no_user in users:
                        no_user.password = get_random_string()
                        groups = [6,0]

#                        if no_user.app_admin:
#                            groups.append(admin_group_ids)
#                        else:
#                            groups.append(user_group_ids)
                        user_data = {
                            'name': no_user.username,
                            'login': no_user.username,
                            'new_password': no_user.password,
                            'user_email': contract.profile.user.email,
                            #'groups_id': groups
                        }
                        no_user.user_id = obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'res.users', 'create', user_data)
                        no_user.state = 'active'
                        no_user.save()
                        user_ids.append(no_user.user_id)
                    instance.state = contract.state
                    if not contract.profile.pay_excent:
                        module_ids = obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'ir.module.module', 'search', [('name', '=', 'eloq_portal')])
                        obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'ir.module.module', 'button_immediate_install', module_ids)
    #creacion de la tarea de vencimiento de contrato
                        stop_data = {
                            'name': instance.db_name,
                            'contract_id': contract.id,
                            'end_date': datetime.datetime.strftime(contract.end_date, '%Y-%m-%d %H:%M:%S'),
                            'user_ids': [(6, 0, user_ids)],
                            'interval': contract.months,
                            'state': contract.state
                        }
                        instance.stop_id = obj_conn.execute(instance.db_name, uid, settings.OPENERP_ADMIN_PASSWORD, 'eloq.contract_stop', 'create', stop_data)
                instance.save()
            c = {'contract': contract,'url_openerp': settings.URL_OPENERP,}
            subject = loader.render_to_string(portal_config.get('create').get('subject_template'), c)
            subject = ''.join(subject.splitlines())
            message = loader.render_to_string(portal_config.get('create').get('email_template'), c)
            msg = EmailMultiAlternatives(subject, message, None, [contract.profile.user.email])
            msg.attach_alternative(message, "text/html")
            msg.send(True)
#             send_mail(subject, message, None, [contract.profile.user.email],fail_silently=True)
        contract.save()



class ProductShareForm(forms.Form):
    product = forms.IntegerField()
    email = forms.EmailField()

    def clean_product(self):
        product = Product.objects.get(id=self.cleaned_data['product'])
        if product:
            return product
        raise forms.ValidationError(_("El producto no es valido."))
