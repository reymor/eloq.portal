from datetime import datetime, timedelta

from django.db import models
from django.contrib.auth.models import User
from django.contrib.comments.moderation import CommentModerator, moderator
from django_countries import CountryField

class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    icon = models.ImageField(upload_to="media")
    url_demo = models.URLField()
    url_video = models.URLField()
    modules = models.CharField(max_length=500)
    admin_groups = models.CharField(max_length=500, blank = True, null = True)
    user_groups = models.CharField(max_length=500, blank = True, null = True)
    bonus = models.FloatField()
    bonus_end = models.IntegerField()
    demo_end = models.IntegerField()
    slug = models.SlugField()

    backup = models.FileField(upload_to="products")
    ficha = models.FileField(upload_to="media")
    ppt = models.FileField(upload_to="media")
    manual = models.FileField(upload_to="media")
    src = models.FileField(upload_to="media")
    def get_absolute_url(self):
        return "/products/"+self.slug
        
    def __unicode__(self):
        return self.name



class ProductImage(models.Model):
    image = models.ImageField(upload_to="media")
    product = models.ForeignKey(Product,related_name="images")
        
    def __unicode__(self):
        return self.image.name
    


class ProductPlan(models.Model):
    instances = models.IntegerField()
    users = models.IntegerField()
    product = models.ForeignKey(Product, related_name="plans")
    price = models.FloatField()
    months = models.IntegerField()
        
    def __unicode__(self):
        return u"Instancias: "+str(self.instances)+", Usuarios: "+str(self.users)+", Precio: "+str(self.price)
    


class PortalProfile(models.Model):
    user = models.ForeignKey(User, unique=True)
    country = CountryField()
    phone = models.CharField(max_length= 50)
    pay_excent = models.BooleanField()
#    products = models.O
    
    def get_absolute_url(self):
        return ('profiles_profile_detail', (), { 'username': self.user.username })
    
    get_absolute_url = models.permalink(get_absolute_url)
    
    def get_time_active(self):
        return (datetime.date(datetime.now()) - datetime.date(self.user.date_joined)).days
    
    def __unicode__(self):
        return self.user.username
User.profile = property(lambda u: PortalProfile.objects.get_or_create(user=u)[0])



class ProductContract(models.Model):
    profile = models.ForeignKey(PortalProfile, related_name="contracts")
    product = models.ForeignKey(Product)
    ammount = models.FloatField()
    discount = models.FloatField()
    price = models.FloatField()
    months = models.IntegerField()
    init_date = models.DateTimeField(blank = True, null = True)
    end_date = models.DateTimeField(blank = True, null = True)
    state = models.CharField(max_length = 20, choices = (
        ('draft', 'Borrador'),
        ('demo', 'Prueba'),
        ('active', 'Activo'),
        ('extra', 'Mes Extra'),
        ('cancel', 'Cancelado'),
    ))
    

    
class ProductContractInstance(models.Model):
    contract = models.ForeignKey(ProductContract, related_name="instances")
    db_name = models.CharField(max_length = 250)
    stop_id = models.IntegerField(blank = True, null = True)
    state = models.CharField(max_length = 20, choices = (
        ('draft', 'Borrador'),
        ('active', 'Activo'),
        ('demo', 'Prueba'),
        ('extra', 'Mes Extra'),
        ('cancel', 'Cancelado'),
    ))
    


class ProductContractInstanceUser(models.Model):
    instance = models.ForeignKey(ProductContractInstance, related_name="users")
    username = models.CharField(max_length = 250)
    password = models.CharField(max_length = 64, blank = True, null = True)
    user_id = models.IntegerField(blank = True, null = True)
    state = models.CharField(max_length = 20, choices = (
        ('draft', 'Borrador'),
        ('active', 'Activo'),
        ('cancel', 'Cancelado'),
    ))
    app_admin = models.BooleanField()
    


class ProductContractPayment(models.Model):
    contract = models.ForeignKey(ProductContract, related_name="payments")
    ammount = models.FloatField()
    source = models.CharField(max_length = 250)
    details = models.CharField(max_length = 250)
    pay_date = models.DateTimeField()
    
    
    
class ProfileBonus(models.Model):
    profile = models.ForeignKey(PortalProfile, related_name="bonus")
    product = models.ForeignKey(Product)
    init = models.DateTimeField()
    end = models.DateTimeField()
    active = models.BooleanField()
    


class ProductShare(models.Model):
    email = models.EmailField()
    product = models.ForeignKey(Product)
    profile = models.ForeignKey(PortalProfile)
    active = models.BooleanField()
    

    
class SlideShow(models.Model):
    image = models.ImageField(upload_to="media")
    text = models.TextField()
    link = models.CharField(max_length=250)
    

    
#Contact Coments
class Contact_Entry(models.Model):
    title = models.CharField(max_length=250)
    body = models.TextField()
    pub_date = models.DateField()
    enable_comments = models.BooleanField()

class EntryModerator(CommentModerator):
    email_notification = True
    enable_field = 'enable_comments'
moderator.register(Contact_Entry, EntryModerator)