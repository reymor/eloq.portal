from django.conf import settings
from django.contrib import admin
from django.conf.urls import url, include, patterns
#from django.views.generic.base import RedirectView

from django.views.generic.base import TemplateView
from products.sitemaps import ProductSitemap
from products.models import Product
from django.views.generic.list import ListView


#from portal.products.models import PortalProfile
#from django.views.generic import ListView
admin.autodiscover()
handler500 = 'django.views.defaults.server_error'
handler404 = 'django.views.defaults.page_not_found'
handler403 = 'django.views.defaults.permission_denied'

sitemaps = {
    'Products': ProductSitemap
}
urlpatterns = patterns('',
    #    Examples:
    #url(r'^$', 'portal.views.home', name='home'),
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^home\.html$', 'products.views.home_view', {'template_name':"home.html"}),
    url(r'^about\.html$', TemplateView.as_view(template_name="about.html")),
    
    url(r'^services\.html$', TemplateView.as_view(template_name="services/index.html")),
    url(r'^services/openerp\.html$', TemplateView.as_view(template_name="services/openerp.html")),
    url(r'^services/consultoria_empresarial\.html$', TemplateView.as_view(template_name="services/consultoria_empresarial.html")),
    url(r'^services/desarrollo_software\.html$', TemplateView.as_view(template_name="services/desarrollo_software.html")),
    url(r'^services/traduccion_subtitulado\.html$', TemplateView.as_view(template_name="services/traduccion_subtitulado.html")),
    url(r'^services/migracion\.html$', TemplateView.as_view(template_name="services/migracion.html")),
    url(r'^services/capacitacion\.html$', TemplateView.as_view(template_name="services/capacitacion.html")),
    url(r'^services/paq-teorico\.html$', TemplateView.as_view(template_name="services/paq-teorico.html")),
    url(r'^services/paq-tecnologico\.html$', TemplateView.as_view(template_name="services/paq-tecnologico.html")),
    url(r'^services/paq-infraestructura\.html$', TemplateView.as_view(template_name="services/paq-infraestructura.html")),
    url(r'^services/identidad_diseno_posicionamiento\.html$', TemplateView.as_view(template_name="services/identidad_diseno_posicionamiento.html")),
    url(r'^contact\.html$', 'products.views.contact_view', {'template_name':"contact.html"}),
    
    url(r'', include('products.urls')),
    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/profile/$', TemplateView.as_view(template_name='accounts/profile.html')),
    url(r'^accounts/login_required\.html$', TemplateView.as_view(template_name='accounts/login_required.html')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^profiles/', include('profiles.urls')),
    
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.index', {'sitemaps': sitemaps}),
    url(r'^sitemap-(?P<section>.+)\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^xmlrpc/$', 'django_xmlrpc.views.handle_xmlrpc', name='xmlrpc'),
    
    url(r'^offline/', include('paypal.standard.ipn.urls')),
    url(r'^support/', include('live_support.urls')),
)

urlpatterns += patterns('',
    url(r'^403/$', 'django.views.defaults.permission_denied'),
    url(r'^404/$', 'django.views.defaults.page_not_found'),
#    url(r'^500/$', 'django.views.defaults.server_error'),
)

if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT})
    )